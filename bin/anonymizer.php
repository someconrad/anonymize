<?php

use Anonymize\Anonymizer;
use Anonymize\Entity\CommandLineParameters;
use Anonymize\Services\ConfigFactory;
use Anonymize\Services\DataTypeService;
use Anonymize\Services\LineParserFactory;

require_once dirname(__DIR__).DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);



$application = new Anonymizer(
    new CommandLineParameters(),
    new ConfigFactory(),
    new LineParserFactory(),
    new DataTypeService()
);


$application->run();

