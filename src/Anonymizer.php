<?php

namespace Anonymize;

use Anonymize\Entity\AnonymizationActions;
use Anonymize\Entity\AnonymizationConfig\AnonymizationColumnConfig;
use Anonymize\Entity\AnonymizationConfig\AnonymizationConfig;
use Anonymize\Entity\CommandLineParameters;
use Anonymize\Entity\Value;
use Anonymize\Exceptions\ConfigValidationException;
use Anonymize\Services\ConfigBuilder\YamlConfig;
use Anonymize\Services\ConfigFactory;
use Anonymize\Services\DataTypeService;
use Anonymize\Services\LineParser\InterfaceLineParser;
use Anonymize\Services\LineParser\MySqlDumpLineParser;
use Anonymize\Services\LineParserFactory;
use InvalidArgumentException;
use RuntimeException;

class Anonymizer
{

    /**
     * @var CommandLineParameters
     */
    private $commandLineParameters;

    /**
     * @var LineParserFactory
     */
    private $lineParserFactory;

    /**
     * @var ConfigFactory
     */
    private $configFactory;

    /**
     * @var DataTypeService
     */
    private $dataTypeService;

    public function __construct(
        CommandLineParameters $commandLineParameters,
        ConfigFactory $configFactory,
        LineParserFactory $lineParserFactory,
        DataTypeService $dataTypeService
    )
    {
        $this->commandLineParameters = $commandLineParameters;
        $this->configFactory = $configFactory;
        $this->lineParserFactory = $lineParserFactory;
        $this->dataTypeService = $dataTypeService;
    }

    private function anonymizeLine($line, AnonymizationConfig $config, InterfaceLineParser $lineParser)
    {
        /** @var MySqlDumpLineParser $lineParser */

        $lineInfo = $lineParser->lineInfo($line);
        if ($lineInfo->isInsert() === false) {
            return $line;
        }

        //truncate action doesnt write inserts
        if ($config->getActionConfig($lineInfo->getTable())->getAction() === AnonymizationActions::TRUNCATE) {
            return null;
        }
        $lineColumns = $lineInfo->getColumns();
        $configColumns = $config->getActionConfig($lineInfo->getTable())->getColumns();

        //Check if config contains all
        if (count($lineColumns) !== count($configColumns)) {
            throw new RuntimeException('Number of columns in table ' . $lineInfo->getTable() . ' doesnt match config.');
        }

        foreach ($lineColumns as $lineColumn) {
            if (!array_key_exists($lineColumn, $configColumns)) {
                throw new RuntimeException('Column not found in config ' . $lineColumn . '');
            }
        }

        //no insert or there is not anonymization required for any of the columns
        if ($lineInfo->isInsert() === false || empty(array_filter($configColumns))) {
            return $line;
        }

        //we have at least one column to anonymize
        $dumpQuery = 'INSERT'.' INTO '.$lineInfo->getTable().' (`';
        $dumpQuery .= implode('`, `', $lineColumns );
        $dumpQuery .= ' VALUES ';
        foreach ($lineParser->getRowFromInsertLine($line) as $row) {
            /** @var Value[] $row */
            $dumpQuery .= '(';
            foreach ($row as $columnIndex => $cell) {
                $columnName = $lineColumns[$columnIndex];
                $dumpQuery .= $this->anonymizeValue($configColumns[$columnName], $cell, array_combine($lineColumns, $row))->getQuotedValue();
                $dumpQuery .= ', ';
            }
            $dumpQuery = substr($dumpQuery, 0, -2);
            $dumpQuery .= '),';
        }
        return substr($dumpQuery, 0, -1).";\n";
    }

    private function anonymizeValue(AnonymizationColumnConfig $columnConfig, Value $value, $row)
    {
        if ($dataType = $this->dataTypeService->getDataType($columnConfig, $row)) {
            $value->setQuotedValue(
                '\'' . addcslashes($this->dataTypeService->anonymizeValue($value, $dataType), '\'') . '\''
            );
        }
        return $value;
    }


    public function run()
    {


        try {

            $this->commandLineParameters->setCommandLineArguments($_SERVER['argv']);
            $this->commandLineParameters->validate();

            /** @var YamlConfig $configService */
            $configService = $this->configFactory->make(
                $this->commandLineParameters->getConfigType(),
                $this->commandLineParameters->getConfigFile()
            );

            $configService->validate();

            $config = $configService->buildConfig();

        } catch (InvalidArgumentException | ConfigValidationException $e) {
            echo 'ERROR: ' . $e->getMessage() . "\n";
            $this->commandLineParameters->help();
            exit(1);
        }


        /** @var MySqlDumpLineParser $lineParser */
        $lineParser = $this->lineParserFactory->chooseLineParser($this->commandLineParameters->getLineParser());

        while ($line = fgets(STDIN)) {
            fwrite(STDOUT, $this->anonymizeLine($line, $config, $lineParser));
        }

        return;

        /*if ($lineParser->isInsertLine($line)) {

            $table = $lineParser->getTableFromInsertLine($line);
            $columns = $lineParser->getColumnsFromInsertLine($line);
            $valuesArray = $lineParser->getValuesFromInsertLine($line);

            echo $table . "==\n";
            if ($this->anonymizationConfigService->hasTable($table)) {
                $action = $this->anonymizationConfigService->tableAction($table);

                if ($action === AnonymizationConfigService::TABLE_ACTION_TRUNCATE) {
                    echo $table . " is truncate \n";
                    //do not output the insert
                    continue;
                }

                if ($action === AnonymizationConfigService::TABLE_ACTION_ANONYMIZE) {


                    if ($this->anonymizationConfigService->hasMultipleColumns($table)) {
                        echo $table . " is multiple\n";
                        foreach ($valuesArray as $values) {
                            foreach ($values as $index => $value) {
                                $currentColumn = $columns[$index];
                                if ($this->anonymizationConfigService->isMultipleColumn($table, $currentColumn)) {
                                    $basedOnKey = $this->anonymizationConfigService->keyForMultipleColumn($table, $currentColumn);

                                    $basedOnKeyIdx = array_search($basedOnKey, $columns);
                                    $basedOnValue = $values[$basedOnKeyIdx];

                                    $dataType = $this->anonymizationConfigService->dataTypeForMultipleColumn($table, $currentColumn, $basedOnKey, $basedOnValue);
                                    echo "table $table - $currentColumn ($value) will be $basedOnKey - $basedOnValue - [$dataType]\n";


                                } elseif ($dataType = $this->anonymizationConfigService->dataTypeSingleColumn($table, $currentColumn)) {

                                    echo "table $table - $currentColumn ($value) will be - [$dataType]\n";
                                } else {
                                    echo "table $table - $currentColumn ($value) NO ANON !\n";
                                }


                            }
                        }

                    } else {
                        foreach ($valuesArray as $values) {
                            foreach ($values as $index => $value) {
                                $currentColumn = $columns[$index];
                                if ($dataType = $this->anonymizationConfigService->dataTypeSingleColumn($table, $currentColumn)) {
                                    echo "table $table - $currentColumn =[$dataType] ($value)  \n";
                                } else {
                                    echo "table $table - $currentColumn DONT-ANON  ($value) \n";
                                }

                            }
                        }
                    }


                }
            } else {
                //verify if all the columns preset in this insert line are defined in the no-anon config file
                foreach ($lineParser->getColumnsFromInsertLine($line) as $column) {
                    if ($this->noAnoymizationConfigService->columnPresent($table, $column) === false) {
                        throw new RuntimeException('`' . $table . '`.`' . $column . '` not found in anonymization configs.');
                    }
                }

                echo "entire table $table doesnt need anon";
            }


        }*/

    }

}