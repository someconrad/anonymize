<?php

namespace Anonymize\Entity;

use Anonymize\DataType\BinaryData;
use Anonymize\DataType\CardData;
use Anonymize\DataType\Credentials;
use Anonymize\DataType\Date;
use Anonymize\DataType\DocumentData;
use Anonymize\DataType\Email;
use Anonymize\DataType\FileName;
use Anonymize\DataType\FreeText;
use Anonymize\DataType\Id;
use Anonymize\DataType\Ip;
use Anonymize\DataType\IpInt;
use Anonymize\DataType\Json;
use Anonymize\DataType\Phone;
use Anonymize\DataType\SensitiveFreeText;
use Anonymize\DataType\Serialized;
use Anonymize\DataType\Url;
use Anonymize\DataType\Username;

class DataTypes {

    public const BANK_DATA = ['BankData', SensitiveFreeText::class];
    public const BINARY_DATA = ['BinaryData', BinaryData::class];
    public const CARD_DATA = ['CardData', CardData::class];
    public const CREDENTIALS = ['Credentials', Credentials::class];
    public const DATE = ['Date', Date::class];
    public const DOCUMENT_DATA = ['DocumentData', DocumentData::class];
    public const EMAIL = ['Email', Email::class];
    public const FILENAME = ['FileName', FileName::class];
    public const FREE_TEXT = ['FreeText', FreeText::class];
    public const ID = ['Id', Id::class];
    public const IP = ['Ip', Ip::class];
    public const IP_INT = ['IpInt', IpInt::class];
    public const JSON = ['Json', Json::class];
    public const PHONE = ['Phone', Phone::class];
    public const SENSITIVE_FREE_TEXT = ['SensitiveFreeText', SensitiveFreeText::class];
    public const SERIALIZED = ['Serialized', Serialized::class];
    public const URL = ['Url', Url::class];
    public const USERNAME = ['Username', Username::class];



}