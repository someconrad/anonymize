<?php

namespace Anonymize\Services;


use Anonymize\DataType\BinaryData;
use Anonymize\DataType\CardData;
use Anonymize\DataType\Credentials;
use Anonymize\DataType\Date;
use Anonymize\DataType\DocumentData;
use Anonymize\DataType\Email;
use Anonymize\DataType\FileName;
use Anonymize\DataType\FreeText;
use Anonymize\DataType\Id;
use Anonymize\DataType\InterfaceDataType;
use Anonymize\DataType\Ip;
use Anonymize\DataType\IpInt;
use Anonymize\DataType\Json;
use Anonymize\DataType\Phone;
use Anonymize\DataType\SensitiveFreeText;
use Anonymize\DataType\Serialized;
use Anonymize\DataType\Url;
use Anonymize\DataType\Username;
use Anonymize\Entity\AnonymizationConfig\AnonymizationColumnConfig;
use Anonymize\Entity\Value;
use RuntimeException;

class DataTypeService
{


    /**
     * @param AnonymizationColumnConfig $anonymizationColumnConfig
     * @param Value[] $row
     * @return InterfaceDataType|null
     */
    public function getDataType(AnonymizationColumnConfig $anonymizationColumnConfig, $row) : ?InterfaceDataType
    {
        if ($anonymizationColumnConfig->getDataType() === false) {
            return null;
        }

        if ($anonymizationColumnConfig->getDataType() === true) {
            $eavAttribute = $row[$anonymizationColumnConfig->getEavAttributeName()]->getValue();
            $eavValues = $anonymizationColumnConfig->getEavAttributeValuesDataType();
            if (array_key_exists($eavAttribute, $eavValues)) {
                $dataType = $eavValues[$eavAttribute];
            }else{
                //todo what happends when script finds a non-defined attribut for eav
                $dataType = 'FreeText';
            }
            return $this->getDataTypeClass($dataType);
        }

        return $this->getDataTypeClass($anonymizationColumnConfig->getDataType());

    }

    public function anonymizeValue(Value $value, InterfaceDataType $dataType) : string
    {
        return $dataType->anonymize($value->getValue());

    }

    private function getDataTypeClass(string $dataType): InterfaceDataType
    {

        switch ($dataType) {
            case 'BankData':
                return new SensitiveFreeText();
                break;
            case 'BinaryData':
                return new BinaryData();
                break;
            case 'CardData':
                return new CardData();
                break;
            case 'Credentials':
                return new Credentials();
                break;
            case 'Date':
                return new Date();
                break;
            case 'DocumentData':
                return new DocumentData();
                break;
            case 'Email':
                return new Email();
                break;
            case 'FileName':
                return new FileName();
                break;
            case 'FreeText':
                return new FreeText();
                break;
            case 'Id':
                return new Id();
                break;
            case 'Ip':
                return new Ip();
                break;
            case 'IpInt':
                return new IpInt();
                break;
            case 'Json':
                return new Json();
                break;
            case 'Phone':
                return new Phone();
                break;
            case 'SensitiveFreeText':
                return new SensitiveFreeText();
                break;
            case 'Serialized':
                return new Serialized();
                break;
            case 'Url':
                return new Url();
                break;
            case 'Username':
                return new Username();
                break;
        }

        throw new RuntimeException('Invalid data type ' . $dataType);

    }

}