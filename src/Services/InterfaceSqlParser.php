<?php

namespace Anonymize\Services;

interface InterfaceSqlParser {

    public function getColumns($insertLine);

    public function getValues($insertLine);

}