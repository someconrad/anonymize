<?php

namespace Anonymize\Services\LineParser;

use Anonymize\Entity\LineInfo;

interface InterfaceLineParser {

    public function lineInfo(string $line) : LineInfo;

    public function getRowFromInsertLine($line);

}