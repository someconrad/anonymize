<?php

namespace Anonymize\Services;

use Anonymize\Services\LineParser\InterfaceLineParser;
use Anonymize\Services\LineParser\MySqlDumpLineParser;

class LineParserFactory {

    public const LINE_PARSER_MYSQL_DUMP = 'mysqldump';


    public function chooseLineParser($configString) : InterfaceLineParser {

        if ($configString === self::LINE_PARSER_MYSQL_DUMP) {
            return new MySqlDumpLineParser();
        }

        throw new \RuntimeException('Invalid line parser config');
    }


}