<?php

namespace Anonymize\Services\ConfigBuilder;

use Anonymize\Entity\AnonymizationConfig\AnonymizationConfig;
use Anonymize\Exceptions\ConfigValidationException;

interface InterfaceConfigBuilder {

    /**
     * @throws ConfigValidationException
     */
    public function validate() : void;

    public function buildConfig() : AnonymizationConfig;

}