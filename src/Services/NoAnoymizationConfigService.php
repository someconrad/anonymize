<?php

namespace Anonymize\Services;


use Symfony\Component\Yaml\Parser;

class NoAnoymizationConfigService
{

    private $data;

    /**
     * @var Parser
     */
    private $parser;

    public function __construct(Parser $yaml)
    {
        $this->parser = $yaml;
    }

    public function loadConfigData($anonymizeFile) {

        if ($this->data === null) {
            $this->data = $this->parser->parseFile($anonymizeFile);
        }
        return $this->data;
    }

    public function columnPresent($table, $column) {
        return array_key_exists($column, $this->data[$table]);
    }


}