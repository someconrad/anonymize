<?php

namespace Anonymize\Services;


use Symfony\Component\Yaml\Parser;

class AnonymizationConfigService {

    private $data;

    private $multipleColumnTables = [];

    private $singleColumnTables = [];

    const TABLE_ACTION_ANONYMIZE = 'anonymize';
    const TABLE_ACTION_TRUNCATE = 'truncate';


    /**
     * @var Parser
     */
    private $parser;

    public function __construct(Parser $yaml)
    {
        $this->parser = $yaml;
    }


    public function loadConfigData($anonymizeFile) {

        if ($this->data === null) {
            $this->data = $this->parser->parseFile($anonymizeFile);

            foreach ($this->data as $table=>$info) {
                if (array_key_exists('Columns', $info)) {
                    foreach ($info['Columns'] as $columnData) {

                        if (isset($columnData['Where'])) {
                            [$whereKey, $whereValue] = explode('=', $columnData['Where'], 2);
                            //TODO validate somehow key/value
                            $this->multipleColumnTables[$table][$columnData['ColumnName']][$whereKey][$whereValue] = $columnData['DataType'];
                        }else{
                            $this->singleColumnTables[$table][$columnData['ColumnName']] = $columnData['DataType'];
                        }
                    }
                }
            }
        }
        return $this->data;
    }

    public function hasMultipleColumns($table) {

        return array_key_exists($table, $this->multipleColumnTables);
    }

    public function hasTable($table) {
        return array_key_exists($table, $this->data);
    }

    public function tableAction($table) {
        return $this->data[$table]['Action'];
    }

    public function isMultipleColumn($table, $column) {
        return array_key_exists($column, $this->multipleColumnTables[$table]);
    }

    public function keyForMultipleColumn($table, $column) {
        $keys = array_keys($this->multipleColumnTables[$table][$column]);
        return $keys[0];
    }

    public function dataTypeForMultipleColumn($table, $column, $key, $value) {
        return $this->multipleColumnTables[$table][$column][$key][$value];
    }

    public function dataTypeSingleColumn($table, $column) {

        if (array_key_exists($table, $this->singleColumnTables) && array_key_exists($column, $this->singleColumnTables[$table])) {
            return $this->singleColumnTables[$table][$column];
        }
        return false;
    }

}