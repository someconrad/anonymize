<?php

//TODO remove
class Settings {

    private $md5Salt;
    private $upperCaseLetterList;
    private $lowerCaseLetterList;
    private $numberList;

    private static $instance;

    private function __construct()
    {
        $this->md5Salt = md5(microtime());

        /** @noinspection NonSecureStrShuffleUsageInspection */
        $this->lowerCaseLetterList = str_shuffle(implode('',range('a','z')));

        /** @noinspection NonSecureStrShuffleUsageInspection */
        $this->upperCaseLetterList = str_shuffle(implode('',range('A','Z')));

        /** @noinspection NonSecureStrShuffleUsageInspection */
        $this->numberList = str_shuffle(implode('',range(0,9)));
    }

    public static function instance() {
        if (!self::$instance instanceof self ) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @return string
     */
    public function getMd5Salt(): string
    {
        return $this->md5Salt;
    }

    /**
     * @return string
     */
    public function getUpperCaseLetterList(): string
    {
        return $this->upperCaseLetterList;
    }

    /**
     * @return string
     */
    public function getLowerCaseLetterList(): string
    {
        return $this->lowerCaseLetterList;
    }

    /**
     * @return string
     */
    public function getNumberList(): string
    {
        return $this->numberList;
    }



}