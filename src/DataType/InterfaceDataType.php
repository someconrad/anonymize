<?php

namespace Anonymize\DataType;

interface InterfaceDataType {
    public function anonymize($value);
}