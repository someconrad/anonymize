<?php

namespace Anonymize\DataType;

class FreeText implements InterfaceDataType
{

    public function anonymize($value)
    {
        return 'FFFF'.$value;
    }


    private function lettersFromHash($word)
    {
        if (strlen($word) > MIN_WORD_LEN) {
            return md5($word . MD5SALT);
        }

        $hash = md5($word . MD5SALT);
        $hash .= $hash . $hash . $hash;
        $letters = str_replace(range(0, 9), '', $hash);
        $numbers = str_replace(range('a', 'z'), '', $hash);
        $ret = '';
        $strlen = strlen($word);
        for ($i = 0; $i < $strlen; $i++) {
            $char = $word[$i];
            if (is_numeric($char)) {
                $ret .= $numbers[$i];
            } elseif (ctype_upper($word[$i])) {
                $ret .= strtoupper($letters[$i]);
            } elseif (ctype_alnum($word[$i])) {
                $ret .= $letters[$i];
            } else {
                $ret .= '' . $word[$i] . '';//$letters[$i];
            }

        }
        return $ret;
    }

}